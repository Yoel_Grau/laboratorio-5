package MiListaDeTareas;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MiListaDeTareasTest {

    @Test
    public void testConstructorYGetters() {
        Tarea tarea = new Tarea("Tarea de prueba");
        assertEquals("Tarea de prueba", tarea.getNombre());
        assertEquals("pendiente", tarea.getEstado());
    }

    @Test
    public void testSetEstado() {
        Tarea tarea = new Tarea("Tarea de prueba");
        tarea.setEstado("en curso");
        assertEquals("en curso", tarea.getEstado());
    }

    private GestionTareas gestionTareas;

    @BeforeEach
    void setUp() {
        gestionTareas = new GestionTareas();
    }

    @Test
    void testAgregarYEmpezarTarea() {
        Tarea tarea = new Tarea("Tarea de prueba");
        gestionTareas.agregarTarea(tarea);
        gestionTareas.empezarTarea();

        assertFalse(gestionTareas.tareasPendientes.contains(tarea));
        assertTrue(gestionTareas.tareasEnCurso.contains(tarea));
        assertEquals("en curso", tarea.getEstado());
    }

    @Test
    void testFinalizarTarea() {
        Tarea tarea = new Tarea("Tarea de prueba");
        gestionTareas.agregarTarea(tarea);
        gestionTareas.empezarTarea();
        gestionTareas.finalizarTarea();

        assertTrue(gestionTareas.tareasFinalizadas.contains(tarea));
        assertEquals("finalizada", tarea.getEstado());
    }

    @Test
    void testNoHayMasTareasPendientes() {
        gestionTareas.empezarTarea();
        assertTrue(gestionTareas.tareasPendientes.isEmpty());
        assertTrue(gestionTareas.tareasEnCurso.isEmpty());
    }

    @Test
    void testNoHayTareasEnProceso() {
        gestionTareas.finalizarTarea();
        assertTrue(gestionTareas.tareasEnCurso.isEmpty());
        assertTrue(gestionTareas.tareasFinalizadas.isEmpty());
    }
}