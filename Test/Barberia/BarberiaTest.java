package Barberia;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BarberiaTest {

    @Test
    public void testEstadoClientesDespuesDeAtencion() {
        Barberia barberia = new Barberia();

        barberia.llegadaCliente("c1");
        barberia.llegadaCliente("c2");
        barberia.llegadaCliente("c3");
        barberia.llegadaCliente("c4");
        barberia.llegadaCliente("c5");
        barberia.llegadaCliente("c6");
        barberia.llegadaCliente("c7");
        barberia.llegadaCliente("c8");

        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();

        String estadoEsperado =
                "c1, en atención\n" +
                        "c2, en atención\n" +
                        "c3, en atención\n" +
                        "c4, en atención\n" +
                        "c5, en atención\n" +
                        "c6, en atención\n" +
                        "c7, en atención\n" +
                        "c8, en atención\n";

        assertEquals(estadoEsperado, barberia.obtenerEstadoClientes(barberia));
    }

    @Test
    public void testEstadoClientesDespuesDeOperaciones() {
        Barberia barberia = new Barberia();

        barberia.llegadaCliente("c1");
        barberia.llegadaCliente("c2");
        barberia.llegadaCliente("c3");
        barberia.llegadaCliente("c4");

        barberia.atenderSiguiente();
        barberia.atenderSiguiente();

        barberia.llegadaCliente("c5");
        barberia.llegadaCliente("c6");

        barberia.atenderSiguiente();
        barberia.atenderSiguiente();

        String estadoEsperado =
                "c1, en atención\n" +
                        "c2, en atención\n" +
                        "c3, en atención\n" +
                        "c4, en atención\n" +
                        "c5, en espera\n" +
                        "c6, en espera\n";

        assertEquals(estadoEsperado, barberia.obtenerEstadoClientes(barberia));
    }




}


