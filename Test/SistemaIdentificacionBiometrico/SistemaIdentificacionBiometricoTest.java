package SistemaIdentificacionBiometrico;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;

class SistemaIdentificacionBiometricoTest {
    private Estacion estacion;

    @BeforeEach
    void Instanciar() {
        estacion = new Estacion();
        centralizador = new Centralizador();
    }

    private Centralizador centralizador;

    @Test
    void testAddYGetRegistrados() {
        Persona persona = new Persona("Juan", 1252, 18);
        estacion.add(persona);
        assertTrue(estacion.getRegistrados().contains(persona));
    }

    @Test
    void testUnicidadDePersonas() {
        Persona persona1 = new Persona("Juan", 1252, 18);
        Persona persona2 = new Persona("Juan", 1252, 20);
        estacion.add(persona1);
        estacion.add(persona2);
        assertEquals(1, estacion.getRegistrados().size());
    }

    @Test
    void testEqualsConMismaIdentificacion() {
        Persona persona1 = new Persona("Juan", 1001, 30);
        Persona persona2 = new Persona("Juan", 1001, 25);
        assertEquals(persona1, persona2);
    }

    @Test
    void testEqualsConDiferenteIdentificacion() {
        Persona persona1 = new Persona("Juan", 1001, 30);
        Persona persona2 = new Persona("Pedro", 1002, 30);
        assertNotEquals(persona1, persona2);
    }

    @Test
    void testHashCodeConsistencia() {
        Persona persona1 = new Persona("Juan", 1001, 30);
        Persona persona2 = new Persona("Juan", 1001, 25);
        assertEquals(persona1.hashCode(), persona2.hashCode());
    }

    @Test
    void testToString() {
        Persona persona = new Persona("Juan", 1001, 30);
        String expected = "Persona{nombre='Juan', identificacion=1001, edad=30}";
        assertEquals(expected, persona.toString());
    }


    @Test
    void testAgregarEstacion() {
        Estacion estacion = new Estacion();
        estacion.add(new Persona("Juan", 1001, 30));
        estacion.add(new Persona("Ana", 1002, 25));

        centralizador.addEstacion(estacion);
        Set<Persona> datosProcesados = centralizador.getDatosProcesados();

        assertEquals(2, datosProcesados.size());
    }

    @Test
    void testAgregarEstacionesConDatosDuplicados() {
        Estacion estacion1 = new Estacion();
        estacion1.add(new Persona("Juan", 1001, 30));

        Estacion estacion2 = new Estacion();
        estacion2.add(new Persona("Juan", 1001, 30));

        centralizador.addEstacion(estacion1);
        centralizador.addEstacion(estacion2);

        Set<Persona> datosProcesados = centralizador.getDatosProcesados();

        assertEquals(1, datosProcesados.size());
    }

}
