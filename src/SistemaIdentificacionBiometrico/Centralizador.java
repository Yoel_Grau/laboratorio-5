package SistemaIdentificacionBiometrico;

import java.util.HashSet;
import java.util.Set;

public class Centralizador {
    private Set<Persona> datosProcesados;

    public Centralizador() {
        this.datosProcesados = new HashSet<>();
    }

    public void addEstacion(Estacion estacion) {
        datosProcesados.addAll(estacion.getRegistrados());
    }

    public void mostrarDatosProcesados() {
        for (Persona persona : datosProcesados) {
            System.out.println(persona);
        }
    }

    public Set<Persona> getDatosProcesados() {
        return datosProcesados;
    }

    public void setDatosProcesados(Set<Persona> datosProcesados) {
        this.datosProcesados = datosProcesados;
    }
}







