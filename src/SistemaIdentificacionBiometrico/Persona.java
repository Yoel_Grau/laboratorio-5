package SistemaIdentificacionBiometrico;

public class Persona {
    private String nombre;
    private int identificacion;
    private int edad;

    public Persona(String nombre, int identificacion, int edad) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.edad = edad;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Persona persona = (Persona) obj;
        return identificacion == persona.identificacion;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(identificacion);
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", identificacion=" + identificacion +
                ", edad=" + edad +
                '}';
    }
}



