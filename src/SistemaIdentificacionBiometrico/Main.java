package SistemaIdentificacionBiometrico;

public class Main {
    public static void main(String[] args) {
        Estacion c1 = new Estacion();
        c1.add(new Persona("Juan", 1252, 18));
        c1.add(new Persona("Raul", 2252, 18));
        c1.add(new Persona("Janeth", 1587, 18));
        c1.add(new Persona("Janeth", 1587, 18));

        Estacion c2 = new Estacion();
        c2.add(new Persona("Juan", 1252, 18));
        c2.add(new Persona("Mery", 3225, 21));
        c2.add(new Persona("Jenny", 2557, 18));
        c1.add(new Persona("Janeth", 1587, 18));

        Estacion c3 = new Estacion();
        c3.add(new Persona("Julio", 2222, 18));
        c3.add(new Persona("Alex", 2558, 18));
        c3.add(new Persona("Janeth", 1587, 18));

        Centralizador cent = new Centralizador();
        cent.addEstacion(c1);
        cent.addEstacion(c2);
        cent.addEstacion(c3);
        cent.mostrarDatosProcesados();
    }
}


