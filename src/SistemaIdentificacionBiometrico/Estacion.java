package SistemaIdentificacionBiometrico;

import java.util.HashSet;
import java.util.Set;

public class Estacion {
    private Set<Persona> registrados;

    public Estacion() {
        this.registrados = new HashSet<>();
    }

    public void add(Persona persona) {
        registrados.add(persona);
    }
    public Set<Persona> getRegistrados() {
        return registrados;
    }
}



