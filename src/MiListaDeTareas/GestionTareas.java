package MiListaDeTareas;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

class GestionTareas {
    public Queue<Tarea> tareasPendientes;
    public Stack<Tarea> tareasEnCurso;
    public Stack<Tarea> tareasFinalizadas;

    public GestionTareas() {
        tareasPendientes = new ArrayDeque<>();
        tareasEnCurso = new Stack<>();
        tareasFinalizadas = new Stack<>();
    }

    public void agregarTarea(Tarea tarea) {
        tareasPendientes.offer(tarea);
    }

    public void mostrarEstado() {
        for (Tarea tarea : tareasFinalizadas) {
            System.out.println(tarea.getNombre() + ", " + tarea.getEstado());
        }
        for (Tarea tarea : tareasEnCurso) {
            System.out.println(tarea.getNombre() + ", " + tarea.getEstado());
        }
        for (Tarea tarea : tareasPendientes) {
            System.out.println(tarea.getNombre() + ", " + tarea.getEstado());
        }
        System.out.println();
    }

    public void empezarTarea() {
        if (!tareasPendientes.isEmpty()) {
            Tarea tarea = tareasPendientes.poll();
            tarea.setEstado("en curso");
            tareasEnCurso.push(tarea);
        } else {
            System.out.println("No hay mas tareas pendientes.");
        }
    }

    public void finalizarTarea() {
        if (!tareasEnCurso.isEmpty()) {
            Tarea tarea = tareasEnCurso.pop();
            tarea.setEstado("finalizada");
            tareasFinalizadas.push(tarea);
        } else {
            System.out.println("No hay tareas en proceso.");
        }
    }
}


