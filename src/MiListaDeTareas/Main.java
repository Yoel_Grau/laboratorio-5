package MiListaDeTareas;

public class Main {
    public static void main(String[] args) {
        GestionTareas gestionTareas = new GestionTareas();

        gestionTareas.agregarTarea(new Tarea("Clase 1: "));
        gestionTareas.agregarTarea(new Tarea("Resolver quiz: "));
        gestionTareas.agregarTarea(new Tarea("Atender cliente: "));
        gestionTareas.agregarTarea(new Tarea("Atender entrevista: "));

        gestionTareas.empezarTarea();
        gestionTareas.mostrarEstado();
        gestionTareas.finalizarTarea();

        gestionTareas.empezarTarea();
        gestionTareas.mostrarEstado();
        gestionTareas.finalizarTarea();

        gestionTareas.empezarTarea();
        gestionTareas.mostrarEstado();
        gestionTareas.finalizarTarea();

        gestionTareas.empezarTarea();
        gestionTareas.mostrarEstado();
        gestionTareas.finalizarTarea();
        gestionTareas.mostrarEstado();
    }
}


