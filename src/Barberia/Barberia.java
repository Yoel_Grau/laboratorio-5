package Barberia;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Barberia {
    private Queue<Cliente> clientesEnEspera;
    private List<Cliente> clientesAtendidos;

    public Barberia() {
        this.clientesEnEspera = new LinkedList<>();
        this.clientesAtendidos = new ArrayList<>();
    }

    public Queue<Cliente> getClientesEnEspera() {
        return new LinkedList<>(clientesEnEspera);
    }


    public List<Cliente> getClientesAtendidos() {
        return new ArrayList<>(clientesAtendidos);
    }


    public void llegadaCliente(String nombreCliente) {
        Cliente cliente = new Cliente(nombreCliente);
        clientesEnEspera.offer(cliente);
        imprimirEstadoClientes();
    }

    public void atenderSiguiente() {
        if (!clientesEnEspera.isEmpty()) {
            Cliente cliente = clientesEnEspera.poll();
            cliente.setEstado("en atención");
            clientesAtendidos.add(cliente);
        }
        imprimirEstadoClientes();
    }

    private void imprimirEstadoClientes() {
        for (Cliente cliente : clientesAtendidos) {
            System.out.println(cliente);
        }
        for (Cliente cliente : clientesEnEspera) {
            System.out.println(cliente);
        }
        System.out.println();
    }

    public String obtenerEstadoClientes(Barberia barberia) {
        StringBuilder estado = new StringBuilder();

        for (Cliente cliente : barberia.getClientesAtendidos()) {
            estado.append(cliente.getNombre()).append(", ").append(cliente.getEstado()).append("\n");
        }
        for (Cliente cliente : barberia.getClientesEnEspera()) {
            estado.append(cliente.getNombre()).append(", ").append(cliente.getEstado()).append("\n");
        }

        return estado.toString();
    }
}







