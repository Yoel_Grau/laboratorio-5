package Barberia;

public class Cliente {
    private String nombre;
    private String estado;

    public Cliente(String nombre) {
        this.nombre = nombre;
        this.estado = "en espera";
    }

    public String getNombre() {
        return nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }
}






