package Barberia;

public class Main {
    public static void main(String[] args) {
        String[] clientes = {"c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8"};

        Barberia barberia = new Barberia();

        System.out.println("Clientes llegando:");
        for (String cliente : clientes) {
            barberia.llegadaCliente(cliente);
        }

        System.out.println("Empieza el proceso de atencion: \n");

        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
        barberia.atenderSiguiente();
    }
}


